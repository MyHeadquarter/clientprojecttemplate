define([
    'backbone',
    'controllers/MainController'
],
  function (Backbone, MainController) {
      'use strict';
      console.log('AppRouter');
      return Backbone.Marionette.AppRouter.extend({
          /* Backbone routes hash */
      appRoutes: {
      },

      initialize: function(options) {
          this.controller = new MainController()
          //add the access_token route
          // this.appRoute(/.+access_token=*([^\&]+)/, 'access_token')
      }
  });
});