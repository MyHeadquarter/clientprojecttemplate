define([
    'backbone',
    'hbs!tmpl/layout/AppLayout_tmpl',
    'reqres'
],
  function (Backbone, ApplayoutTmpl, reqres) {
      'use strict';

      /* Return a Layout class definition */
      return Backbone.Marionette.Layout.extend({

          initialize: function() {
              //determine if user is logged in
              this._isloggedin = reqres.request('account:isloggedin') || false
              if (this._isloggedin) return this._view = new AccountNavLayout()

          },

          template: ApplayoutTmpl,


          /* Layout sub regions */
          regions: {
              page_content: '#page-content',
              account_nav: '#account_nav',
              task_list_left_menu_region: '#task-list-left-menu'
          },

          /* ui selector cache */
          ui: {},

          /* Ui events hash */
          events: {},

          /* on render callback */
          onRender: function() {
              this._view && this.account_nav.show(this._view)
          }
      
      });

  });