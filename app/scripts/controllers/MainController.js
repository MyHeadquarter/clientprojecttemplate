/// <reference path="TodayController.js" />
define([
    'backbone',
    'regionManager',
    'reqres',
    'views/layout/AppLayout',
    'qs',
    'vent'
],
  function (Backbone, regionManager, reqres, AppLayout, qs) {
      'use strict';
      return Backbone.Marionette.Controller.extend({

          initialize: function (options) {
              /*
              get the application region manager, which is available on the global reqres
               */
              this._regionManager = reqres.request('app:regionmanager') || new Backbone.Marionette.RegionManager()
              /*
              get the app layout region which will be used for loading the main application view
               */
              this._region = this._regionManager.get('app_layout_region')

              this._appLayout = reqres.request('app:layout')
          },
          _setCurrentMainController: function (controller) {
              this._currentController && this._currentController.close()
              this._currentController = controller
          }
      });

  });