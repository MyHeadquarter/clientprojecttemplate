define([
		'backbone',
		'communicator',
		'routers/MainRouter',
		'reqres',
		'views/layout/AppLayout'
	],

	function(Backbone, Communicator, MainRouter, reqres, AppLayout) {
		'use strict';

		var App = new Backbone.Marionette.Application();

		App._getRegionManger = function() {
			return this._regionManager
		}
		/* Add application regions here */
		App.addRegions({
			app_layout_region: '#app_layout'
		});
		App._getAppLayout = function() {
			return this._appLayout
		}

		/* Add initializers here */
		App.addInitializer(function() {
			this._appLayout = new AppLayout()
			this.app_layout_region.show(this._appLayout)
			reqres.setHandler('app:layout', this._getAppLayout.bind(this))
			reqres.setHandler('app:regionmanager', this._getRegionManger.bind(this))
			this._router = new MainRouter();
			Backbone.history.start();
		});
		return App;
	});