## Setup
To get started, first clone the repo: 

    git clone https://<username>@bitbucket.com/MyHeadquarter/ClientProjectTemplate.git

make sure you have node 10.x.x

run the following to prepare your development environment

    npm install

    npm install -g grunt-cli grunt bower

    bower install

## Development Server

To start your development server run:

    grunt

This will start your development server on port 9000. It is set to livereload whenever any of the files in your project change.

## Styles

in this file:

    ./app/styles/main.less

you should find a list of imported styles like so:

    @import "../panels.css";
    @import "../navbar.less";

these files may be of either css or less format. You can create a new file for each view you might want to generate styles for. Simply put it in the styles folder and add an import statement to the main.less shown above and when you refresh index.html the less files will be compiled dynamically along with any css/less code you import into main.less.

## Marionette File Generator

*Remember to use --create-all to generate all files in addition to the router/controller/etc.

Create routers
--------------
You can generate routers too with

    $ yo marionette:router router-name



Create model
------------
To add a Backbone model to the project use the model generator like this

    $ yo marionette:model model-name

Or to inherit from an existing model

    $ yo marionette:model model-name --inherit model-name



Create collection
-----------------
To add a Backbone collection to the project use collection generator

    $ yo marionette:collection collection-name

You can link the collection with an existent model

    $ yo marionette:collection collection-name model-name

Or may be you want to create both, model and collection on one step

    $ yo marionette:collection collection-name --model model-name --create-all

Or you may want to inherit from another collection

    $ yo marionette:collection collection-name --model model-name --inherit collection-name --create-all




Create views
------------
Backbone works with view definitions, to create one use this command. It is recommended to use Marionette views instead of the standard Backbone view

    $ yo marionette:view view-name




Create item views
------------
Create a Marionette ItemView and link to an existing template at location templates/[template-location]

    $ yo marionette:itemview view-name 

You may want to inherit from another itemview

    $ yo marionette:itemview view-name --inherit view-name

Or maybe you want to create both, itemView and template on one step

    $ yo marionette:itemview view-name --create-all



Create collection views
------------
Create a Marionette CollectionView that is associated to an existing itemview

    $ yo marionette:collectionview view-name --itemview itemview-name

Or inherit from another collectionview

    $ yo marionette:collectionview view-name --itemview itemview-name --inherit view-name

Or maybe you want to create both, itemview (with template) and collectionview.

    $ yo  marionette:collectionview view-name --itemview itemview-name --create-all




Create composite views
------------
Create a Marionette CompositeView

    $ yo marionette:compositeview view-name --itemview itemview-name

Or inherit from another CompositeView

    $ yo marionette:compositeview view-name --itemview itemview-name --inherit view-name

Or maybe you want to create all, itemview and compositeview and both templates. 

    $ yo marionette:compositeview view-name --itemview itemview-name --create-all




Create regions
------------
Create a Marionette Region

    $ yo marionette:region region-name

Or inherit from another Region

    $ yo marionette:region region-name --inherit region-name




Create layouts
------------
Create a Marionette Layout and link to an existing template at location templates/[template-location]

    $ yo marionette:layout layout-name

Or inherit from another layout

    $ yo marionette:layout layout-name --inherit layout-name

Or maybe you want to create both, Layout and template on one step

    $ yo marionette:layout layout-name --create-all



Create controller
------------
Create a Marionette Controller

    $ yo marionette:controller controller-name

Or inherit from another Controller

    $ yo marionette:controller controller-name --inherit controller-name


Create templates
------------
Create a handle bars tmpl

    $ yo marionette:tmpl tmpl-name --tmplLocation tmpl-location
