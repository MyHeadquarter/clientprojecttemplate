define(function() {
	'use strict';

	/* return an array of specs to be run */
	return {
		specs: [
		'spec/controllers/MainController.js',
		'spec/routers/MainRouter.js',
		'spec/views/layout/AppLayout.js'
		]
	};
});
